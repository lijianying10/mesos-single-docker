FROM ubuntu:14.04.4

MAINTAINER KiwenLau <kiwenlau@gmail.com>

# Setup mesosphere repository
RUN sudo apt-key adv --keyserver keyserver.ubuntu.com --recv E56151BF && \
DISTRO=$(lsb_release -is | tr '[:upper:]' '[:lower:]') && \
CODENAME=$(lsb_release -cs) && \
echo "deb http://repos.mesosphere.io/${DISTRO} ${CODENAME} main" | sudo tee /etc/apt/sources.list.d/mesosphere.list && \
sudo apt-get -y update && apt-get -y install vim curl && curl -fLsS https://get.docker.com/ | sh &&  apt-get install -y software-properties-common && add-apt-repository ppa:webupd8team/java && echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections && apt-get update -y && apt-get install -y oracle-java8-installer oracle-java8-set-default mesos marathon && echo 'docker,mesos' > /etc/mesos-slave/containerizers && echo '5mins' > /etc/mesos-slave/executor_registration_timeout

# Add script for start the mesos/marathon cluster
ADD start-cluster.sh /root/start-cluster.sh

CMD 'bash /root/start-cluster.sh'; bash

