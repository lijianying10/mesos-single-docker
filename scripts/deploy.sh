#!/bin/bash

#REG=docker.elenet.me/jianying.li
#REG=index.alauda.cn/philo
REG=afafaf.newb.xyz

docker -H unix:///var/run/system-docker.sock pull $REG/mesos:ubt
docker -H unix:///var/run/system-docker.sock tag $REG/mesos:ubt mesos:ubt

docker pull $REG/calico-node:v0.20.0
docker pull $REG/calico-node-libnetwork:v0.8.0
docker tag $REG/calico-node-libnetwork:v0.8.0 calico/node-libnetwork:v0.8.0
docker tag $REG/calico-node:v0.20.0 calico/node:v0.20.0

docker -H unix:///var/run/system-docker.sock pull  index.tenxcloud.com/philo/etcd:v233
docker -H unix:///var/run/system-docker.sock tag index.tenxcloud.com/philo/etcd:v233 etcd:v2.3.3
