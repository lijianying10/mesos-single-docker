yum install -y bridge-utils
bridge=systemdocker
net=10.0.0.11/24
sudo brctl addbr $bridge
sudo ip addr add ${net:-"10.20.30.1/24"} dev $bridge
sudo ip link set dev $bridge up
