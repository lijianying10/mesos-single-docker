HostIP=192.168.56.112
docker -H unix:///var/run/system-docker.sock run -it --net=host -h $HostIP  --name marathon -d -w /root -v /var/run/docker.sock:/var/run/docker.sock --privileged mesos:ubt
docker -H unix:///var/run/system-docker.sock run -it -p 4001:4001 -p 2380:2380 -p 2379:2379 -d --name etcd etcd:v2.3.3\
 -name etcd0 \
 -advertise-client-urls http://${HostIP}:2379,http://${HostIP}:4001 \
 -listen-client-urls http://0.0.0.0:2379,http://0.0.0.0:4001 \
 -initial-advertise-peer-urls http://${HostIP}:2380 \
 -listen-peer-urls http://0.0.0.0:2380 \
 -initial-cluster-token etcd-cluster-1 \
 -initial-cluster etcd0=http://${HostIP}:2380 \
 -initial-cluster-state new

systemctl restart docker
