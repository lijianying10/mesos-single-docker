#!/bin/bash


function StartZK(){
    # Start ZooKeeper
    echo "Start ZooKeeper..."
    exec sudo service zookeeper restart >>/dev/null & 
}

function StartMaster(){
    # Start Mesos master
    echo "Start Mesos master..."
    exec /usr/sbin/mesos-master --zk=$ZK --quorum=1 --work_dir=/var/lib/mesos --log_dir=/log/mesos  >>/dev/null 2>&1 &
    
    # Start Marathon
    echo "Start Marathon..."
    exec sudo service marathon start >>/dev/null &
}

function JudgeZK(){
    if [[ $ZK == '' ]]; then
       echo 'need zk address link to mesos server'
       exit 1
    fi
}

function StartSlave(){

    # Start Mesos slave
    echo "Start Mesos slave..."
    exec /usr/sbin/mesos-slave --containerizers=docker,mesos --master=$ZK --log_dir=/log/mesos >>/dev/null 2>&1 &
}


if [[ $ROLE = 'master' ]]; then
    JudgeZK
    StartMaster
elif [[ $ROLE = 'slave' ]]; then
    JudgeZK
    StartSlave
elif [[ $ROLE = 'zk' ]]; then
    StartZK
else
    StartZK
    ZK='zk://127.0.0.1:2181/mesos'
    StartMaster
    StartSlave
fi

